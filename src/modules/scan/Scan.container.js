import React from "react";

export default ({ listRequest, listContact }) => {
  let arr1 = [];
  let arr2 = [];
  for (let i = 0; i < 9; i++) {
    arr1[i] = {
      id: i,
      name: `name ${i}`,
      image: require("../../assets/1.png"),
    };
  }

  for (let i = 0; i < 9; i++) {
    arr2[i] = {
      id: i,
      image: require("../../assets/1.png"),
      name: `name ${i}`,
    };
  }

  listRequest = arr1;
  listContact = arr2;

  // console.log(listRequest);

  return (
    <div style={{ paddingLeft: 100 }}>
      Avatar
      <img
        src={require("../../assets/1.png")}
        style={{ height: 70, width: 70 }}
      />
      <button style={{ fontSize: 80, borderWidth: 1 }}>Scan</button>
      <div style={{ paddingTop: 100 }}>
        <span>List Request</span>
        {!!listRequest && (
          <div>
            {listRequest.map((e) => (
              <React.Fragment>
                {e.name}
                <img src={e.image} style={{ height: 50, width: 50 }} />
              </React.Fragment>
            ))}
          </div>
        )}
        <span>List Contact</span>

        {!!listContact && (
          <div>
            {listContact.map((e) => (
              <React.Fragment>
                {e.name}
                <img src={e.image} style={{ height: 50, width: 50 }} />
              </React.Fragment>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};
