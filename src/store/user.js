import { action, thunk } from "easy-peasy";
import {
  login,
  sendOtp,
  scan,
  getUserProfile,
  getListContact,
  getListRequest,
} from "./service";
import { setItem, getItem, removeAll } from "../utils/localStorage";

export default {
  token: null,
  userProfile: {},
  listContact: [],
  listRequest: [],

  setUserProfile: action(async (state, payload) => {
    state.userProfile = payload.userProfile;
  }),

  setLogin: action((state, payload) => {
    state.token = payload.token;
  }),

  setListContact: action((state, payload) => {
    state.listContact = payload.listContact;
  }),

  setListRequest: action((state, payload) => {
    state.listRequest = payload.listRequest;
  }),

  getUserProfile: thunk(async (actions, payload) => {
    try {
      const result = await getUserProfile({ id: payload.id });
      actions.setUserProfile(result);
    } catch (error) {
      alert(error.message);
    }
  }),

  getListRequest: thunk(async (actions, payload) => {
    try {
      const result = await getListRequest();
      actions.setListRequest(result);
    } catch (error) {}
  }),

  getListContact: thunk(async (actions, payload) => {
    try {
      const result = await getListContact();
      actions.setListContact(result);
    } catch (error) {}
  }),

  login: thunk(async (actions, payload) => {
    const { phone, otp } = payload;
    try {
      const result = await login({ phone, otp });
      actions.setLogin(true);
      setItem("@token", result.token);
      alert("login success");
      return true;
    } catch (error) {
      actions.setLogin(false);
      alert(error.message);
      return false;
    }
  }),

  logout: thunk(async (actions, payload) => {
    actions.setLogin(false);
    await removeAll();
  }),
  sendOtp: thunk(async (actions, payload) => {
    const { phone } = payload;
    try {
      const result = await sendOtp({ phone });
      return result;
    } catch (error) {
      alert(error.message);
      return false;
    }
  }),
};
