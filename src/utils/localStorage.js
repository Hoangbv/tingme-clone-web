export const getItem = async (key) => {
  try {
    const value = await localStorage.setItem(key);
    if (value) {
      return JSON.parse(value);
    }
  } catch (e) {
    console.error(e);
  }
  return undefined;
};

export const removeItem = async (key) => {
  try {
    await localStorage.removeItem(key);
  } catch (e) {
    console.error(e);
  }
};

export const removeAll = async () => await localStorage.clear();

export const setItem = async (key, value) => {
  try {
    const json = JSON.stringify(value);
    await localStorage.setItem(key, json);
  } catch (e) {
    console.error(e);
  }
};
