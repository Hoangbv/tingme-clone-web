import React, { useCallback } from "react";
import { useStoreActions, useStoreState } from "easy-peasy";
import { useHistory } from "react-router-dom";
import OTPComponent from "./OTP.component";

export default ({}) => {
  const { phone } = useStoreState((states) => states.user);
  const { login } = useStoreActions((actions) => actions.user);
  const { push } = useHistory();

  const onSubmit = useCallback(async ({ otp }) => {
    const result = await login({ phone, otp });
    if (result) {
      push("./scan");
    }
  }, []);

  return <OTPComponent onSubmit={onSubmit} />;
};
