import React, { useState, useCallback } from "react";
import { useStoreActions, useStoreState } from "easy-peasy";
import { useHistory } from "react-router-dom";
import AuthComponent from "./Auth.component";

export default ({}) => {
  const { sendOtp } = useStoreActions((actions) => actions.user);
  const { push } = useHistory();

  const onSubmit = async ({ phone }) => {
    const result = await sendOtp({ phone });
    console.log("dadadasadadsaß");
    if (result) {
      push("./otp");
    }
  };

  return <AuthComponent onSubmit={onSubmit} />;
};
