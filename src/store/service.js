import createGraphqlClient from "../utils/createGraphqlClient";
import gql from "graphql-tag";

const url = "http://192.168.1.16:3001/api";

const userApiClient = createGraphqlClient(`${url}/user`);

const locationApiClient = createGraphqlClient(`${url}/location`);

export const getUserProfile = ({ id }) => {};

export const getListContact = () => {};

export const getListRequest = () => {};

export const login = async ({ phone, otp }) => {
  const query = gql`
    query($phone: String, $otp: String) {
      login(phone: $phone, otp: String)
    }
  `;

  const result = await userApiClient.query({
    query,
    variables: { otp, phone },
  });
};

export const sendOtp = async ({ phone }) => {
  const query = gql`
    query($phone: String) {
      requestOtp(phone: $phone)
    }
  `;

  try {
    const result = await userApiClient.query({ query, variables: { phone } });
    return result.data.requestOtp;
  } catch (error) {
    console.log(error);
  }
};

export const scan = ({ lat, lng }) => {};
