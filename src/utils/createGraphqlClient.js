import ApolloClient from "apollo-boost";
import { getItem } from "../utils/localStorage";

export default (uri) => {
  const token = getItem("@token");
  const client = new ApolloClient({
    uri,
    request: (operation) => {
      const headers = {};
      if (token) {
        headers["x-access-token"] = token;
      }
      operation.setContext({ headers });
    },
  });
  client.defaultOptions = { query: { fetchPolicy: "network-only" } };
  return client;
};
