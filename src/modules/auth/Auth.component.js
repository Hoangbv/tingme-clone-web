import React, { useState } from "react";

export default ({ onSubmit }) => {
  const [phone, setPhone] = useState(null);

  return (
    <div>
      <span>phone:</span>
      <input value={phone} onChange={(e) => setPhone(e.target.value)} />
      <button
        style={{ border: "solid 1px" }}
        onClick={() => onSubmit({ phone })}
      >
        login
      </button>
    </div>
  );
};
