import React from "react";
import { StoreProvider } from "easy-peasy";
import store from "../store";
import App from "./App";

export default () => {
  return (
    <StoreProvider store={store}>
      <App />
    </StoreProvider>
  );
};
