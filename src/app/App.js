import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Scan from "../modules/scan/Scan.container";
import Auth from "../modules/auth/Auth.container";
import ChatList from "../modules/chat/ChatList";
import Profile from "../modules/profile/Profile";
import OTP from "../modules/auth/OTP.container";
import ChatBox from "../modules/chat/ChatBox";

export default function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/"></Route>
        <Route path="/auth">
          <Auth />
        </Route>
        <Route path="/otp">
          <OTP />
        </Route>
        <Route path="/scan">
          <Scan />
        </Route>
        <Route path="/profile">
          <Profile />
        </Route>
        <Route path="/chat-box">
          <ChatBox />
        </Route>
        <Route path="/chat-list">
          <ChatList />
        </Route>
      </Switch>
    </Router>
  );
}
