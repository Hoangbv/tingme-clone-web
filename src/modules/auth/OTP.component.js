import React, { useState, useCallback } from "react";

export default ({ onSubmit }) => {
  const [otp, setOtp] = useState(null);
  const [otpValid, setOTPvalid] = useState(true);

  const checkOtpValid = useCallback(async () => {
    if (otp) {
      const condition = otp.length === 4;
      if (!condition) {
        setOTPvalid(false);
      } else {
        setOTPvalid(true);
      }
    }
  }, [otp]);

  return (
    <div>
      <span>Nhập otp để đăng nhập</span>
      <input
        value={otp}
        onBlurCapture={() => onSubmit({ otp })}
        onBlur={() => checkOtpValid()}
        onChange={(e) => setOtp(e.target.value)}
      />
      <span>{otpValid === false && "OTP không đúng"}</span>
    </div>
  );
};
